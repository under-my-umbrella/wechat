import express from 'express'
//引入路由模块
import useRouter from '../router/index.js'
//设置跨域
import cors from 'cors'
import path from 'path'
import { limiter } from '../utils/limiter.js'

class App {
    constructor() {
        this.app = express();
        this.setupStatic();
        this.setupRoutesBefor();
        this.setupRoutes();
    }

    // 路由前配置
    setupRoutesBefor() {
        this.app.use(cors());
        this.app.use(limiter);
        this.app.use(express.json());
    }

    setupRoutes() {
        this.app.use(useRouter);
    }
    setupStatic() {
        // 指定静态资源目录
        this.app.use(express.static(path.join(process.cwd(), 'src/uploads')));
    }

    startServer(port) {
        this.app.listen(port, () => {
            console.log(`Example app listening on port ${port}!`);
        });
    }
}

export default App;