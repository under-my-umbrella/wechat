import fs from 'fs';
import jsyaml from 'js-yaml';
import knex from 'knex';
import path from 'path';
const yaml = fs.readFileSync(path.join(process.cwd(), 'src/db/db.config.yaml'), 'utf-8')
const config = jsyaml.load(yaml)

const db = knex({
    client: 'mysql2',
    connection: config.db,
})

// db.schema.createTableIfNotExists('voice', function (table) {
//     table.increments('id').primary();
//     table.integer('user_id');
//     table.string('telephone', 20).notNullable();
//     table.string('voice_url', 255).notNullable();
// })
//     .then(() => {
//         console.log('创建voice表成功')
//     })
//     .catch(err => {
//         console.log('创建voice表失败', err.message)
//     })


export default db;