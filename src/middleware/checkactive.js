import db from '../db/index.js'
import { respone } from '../utils/respone.js'

// 验证账号是否激活,此中间件只对登录接口有效，在别的接口中不可以使用此中间件
export async function checkactive(req, res, next) {
    const { telephone } = req.body;
    // 判断是否传了手机号
    if (telephone) {
        // 查询数据库
        const user = await db('user').where({ telephone }).first();
        if (!user) return respone(res, 402, '手机号不存在');
        // 判断是否激活
        const { active } = user;
        if (active === 0) return respone(res, 403, '你的账号已经被冻结，暂时无法使用');
        next();
    } else {
       return respone(res, 401, '请输入手机号');
    }
}