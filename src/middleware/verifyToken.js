import jwt from 'jsonwebtoken';
import { respone } from '../utils/respone.js';
import db from '../db/index.js'
// 中间件函数，用于验证 Token
export async function verifyToken(req, res, next) {
    // 从请求头中提取 Token
    const token = req.headers['authorization'];

    // 检查 Token 是否存在
    if (!token) {
        return respone(res, 401, 'Token 不存在', null);
    }

    // 验证 Token 是否以 Bearer 开头
    if (!token.startsWith('Bearer ')) {
        return respone(res, 401, 'Token 格式错误', null);
    }

    // 去除 Bearer 并提取出真正的 Token 值
    const authToken = token.split(' ')[1];

    // 验证 Token 的有效性
    jwt.verify(authToken, '139766', async (err, decoded) => {
        if (err) {
            if (err.name === 'TokenExpiredError') {
                return respone(res, 401, 'Token 已过期', null);
            } else {
                return respone(res, 401, 'Token 无效', null);
            }
        }
        // 如果 Token 有效，将解码后的用户信息附加到请求对象中，以便后续路由可以使用
        req.user = decoded;
        // 继续执行下一个中间件
        const { telephone } = decoded;
        const user = await db('user').where({ telephone }).first();
        const { active } = user;
        if (active === 0) return respone(res, 403, '该账号已被封禁，暂时无法使用', null);
        next();
    });
}