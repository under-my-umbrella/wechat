import { respone } from '../utils/respone.js'

export async function checkallactive(req, res, next) {
    const { telephone } = req.user;
    if (!telephone) return respone(res, 401, '请登录');
    next();
}