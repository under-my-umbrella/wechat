import multer from "multer";
import path from "path";

// 配置multer存储引擎   
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'src/uploads')  //指定文件保存路径
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))  //指定文件名
    }
})

// 创建multer实例
export const uploads = multer({ storage: storage })