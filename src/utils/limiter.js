import rateLimit from 'express-rate-limit'

// 创建限速器
export const limiter = rateLimit({
  // 限制每分钟只能请求10次
  windowMs: 60 * 1000, // 1 分钟
  max: 30, // 限制 1 分钟内最多只能请求 10 次
  message: "请求频率过快，请稍后再试"
})
