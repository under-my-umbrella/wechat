//二次封装axios
import axios from 'axios'

// 创建axios实例
const request = axios.create({
    baseURL: 'https://api.weixin.qq.com',
    timeout: 5000
})

//创建拦截器
request.interceptors.request.use(config => {
    return config;
})

// 添加响应拦截器
request.interceptors.response.use(res => {
    return res.data;
})

//暴露出去
export default request;