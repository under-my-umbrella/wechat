import path from 'path';

// 根据文件扩展名判断文件类型
export function getFileType(filePath) {
    const extension = path.extname(filePath).toLowerCase();
    switch (extension) {
        case '.jpg':
        case '.jpeg':
        case '.png':
            return 'image/jpeg';
        case '.mp3':
        case '.wav':
            return 'audio/mpeg';
        default:
            return null; // 未知类型
    }
}