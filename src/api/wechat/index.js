//引入二次封装好的axios实例
import request from '../../utils/request.js'


//用于处理微信相关的所有请求
export const getAccessToken = (appID, appSecret) => {
    return new Promise((resolve, reject) => {
        request.get(`/cgi-bin/token?grant_type=client_credential&appid=${appID}&secret=${appSecret}`)
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                reject(err);
            })
    })
}

//用于微信授权
export const getCode = (appID, redirect_uri) => {
    return new Promise((resolve, reject) => {
        request.get()
    })
}

