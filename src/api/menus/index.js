//引入二次封装好的axios实例
import request from '../../utils/request.js'

//GET https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN

// 删除菜单
export const deleteMenus = (access_token) => {
    return new Promise((resolve, reject) => {
        request.get('/cgi-bin/menu/delete?access_token=' + access_token)
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
    })
}

// 创建菜单
export const createMenus = (access_token, menus) => {
    return new Promise((resolve, reject) => {
        request.post('/cgi-bin/menu/create?access_token=' + access_token, menus)
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
    })
}