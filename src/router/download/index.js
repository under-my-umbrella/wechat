import express from "express";
import path from 'path';
import { fileURLToPath } from 'url';
const __dirname = path.dirname(fileURLToPath(import.meta.url));
const router = express.Router();

router.get('/download', (req, res) => {
    const filePath = path.join(__dirname, '../../apk', 'feiyu.apk');
    res.sendFile(filePath);
})

export default router;