//引入express
import express from "express";
import wechatRouter from "./wechat/index.js";
import userRouter from "./user/index.js";
import uploadRouter from "./upload/index.js"
import downloadRouter from '../router/download/index.js'


const router = express.Router()

router.use('/wechat', wechatRouter);
router.use('/user', userRouter);
router.use('/upload', uploadRouter);
router.use('/download', downloadRouter);







//暴露路由
export default router;